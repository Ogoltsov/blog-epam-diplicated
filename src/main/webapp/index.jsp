<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="blog" type="com.epam.ok.blog.model.Blog"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Blog</title>
</head>
<body>
<div>
    <hr/>
    <h1 style="text-align: center">${blog.title}</h1>

    <h3 style="text-align: center">${blog.description}</h3>
    <hr/>
</div>
<h3>Posts: ${blog.posts.size()}</h3>
<hr/>
<div>
    <c:forEach items="${blog.posts}" var="post">
        <h3>${post.title} : ${post.likes}</h3>

        <div><b>Author:</b> ${post.author.firstName} ${post.author.lastName}<br/> <b>E-mail:</b> ${post.author.email}
        </div>
        <div><b>Created:</b> ${post.creationDate}</div>
        <div><p style="text-align: justify"><b>Text:</b> <br/> ${post.body}</p></div>
        <hr/>
    </c:forEach></div>
<div>
</div>

</body>
</html>
