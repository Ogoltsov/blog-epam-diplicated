package com.epam.ok.blog.model.IO;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Output {

    public String path = null;

    public void setPath(String path) {
        this.path = path;
    }

    public void Write(String st) throws IOException {

        OutputStream outputStream = new FileOutputStream(path);
        outputStream.write(st.getBytes());
        outputStream.close();
    }
}
