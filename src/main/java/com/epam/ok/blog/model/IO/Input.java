package com.epam.ok.blog.model.IO;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Input {
    public String path;

    public String text = null;

    public void setPath(String path) {
        this.path = path;
    }

    public void Read() throws IOException {
        InputStream inputstream = new FileInputStream(path);

        int data = inputstream.read();
        char content;
        while (data != -1) {
            content = (char) data;
            text += content;
            data = inputstream.read();
        }
        inputstream.close();
    }
}
