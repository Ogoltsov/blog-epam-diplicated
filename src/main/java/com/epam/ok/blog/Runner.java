package com.epam.ok.blog;

import com.epam.ok.blog.model.Blog;
import com.epam.ok.blog.model.Post;
import com.epam.ok.blog.model.factory.BlogFactory;
import org.boon.Boon;

import java.util.Locale;
import java.util.ResourceBundle;

public class Runner {

    public static void main(String[] args) {

        Locale locale = Locale.getDefault();
        System.out.println(locale);
        ResourceBundle resourceBundle = ResourceBundle.getBundle("messages");

        Blog blog = BlogFactory.create();
        String s = Boon.toPrettyJson(blog);
        System.out.println(s);
    }
}
